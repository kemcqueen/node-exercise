const axios = require('axios');

const SWAPI_PEOPLE = 'https://swapi.dev/api/people';

exports.list = (req, res, next) => {
    axios.get(SWAPI_PEOPLE)
        .then((r) => {
            const {count, results: {length}} = r.data;
            const totalPages = Math.ceil(count / length);

            return Promise.all([...Array(totalPages).keys()]
                .map((i) => axios.get(`${SWAPI_PEOPLE}?page=${i + 1}`)));
        })
        .then((pages) => {
            let allPeople = [];
            for (const page of pages) {
                allPeople = [...allPeople, ...page.data.results];
            }
            allPeople.sort((p1, p2) => p1.name.localeCompare(p2.name));
            res.json(allPeople);
        })
        .catch((err) => {
            console.log(err);
            next(err);
        });
};