const axios = require('axios');

const SWAPI_PLANETS = 'https://swapi.dev/api/planets';

exports.list = (req, res, next) => {
    axios.get(SWAPI_PLANETS)
        .then((response) => {
            const {count, results: {length}} = response.data;
            const totalPages = Math.ceil(count / length);
            return Promise.all([...Array(totalPages).keys()].map((i) => axios.get(`${SWAPI_PLANETS}?page=${i + 1}`)));
        })
        .then((pages) => {
            let allPlanets = [];
            for (const page of pages) {
                allPlanets = [...allPlanets, ...page.data.results];
            }
            return Promise.all(allPlanets.map((planet) => populateResidents(planet, next)));
        })
        .then((planets) => {
            res.json(planets.sort((p1, p2) => p1.name.localeCompare(p2.name)));
        })
        .catch((err) => {
            console.log(err);
            next(err);
        })
};

const populateResidents = (planet, next) => {
    return Promise.all(planet.residents.map((resident) => axios.get(resident)))
        .then((responses) => {
            planet.residents = responses.map((residentResponse) => residentResponse.data.name);
            return planet;
        })
        .catch((err) => {
            console.log(err);
            next(err);
        })
};