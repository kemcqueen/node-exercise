var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  res.render('index', { title: 'Node.js Exercise (Featuring Star Wars API)' });
});

module.exports = router;
